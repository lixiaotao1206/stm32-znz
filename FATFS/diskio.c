/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */

#include "string.h"
#include "gd25q64.h"
#include "sdcard_sdio.h"
/* Definitions of physical drive number for each drive */
// #define DEV_RAM					0	/* Example: Map Ramdisk to physical drive 0 */
// #define DEV_MMC					1	/* Example: Map MMC/SD card to physical drive 1 */
// #define DEV_USB					2	/* Example: Map USB MSD to physical drive 2 */
#define DEV_SPI_NORFLASH		0	/* Example: Map SPI norFlash to physical drive 1 */
#define DEV_SDCARD_MMC			1

#define SD_BLOCKSIZE     		512 

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat = STA_NOINIT;

	switch (pdrv) {
		case DEV_SPI_NORFLASH :
			if (SPI_FLASH_ReadID() == sFLASH_ID) {
				stat = 0;
			} else {
				stat = STA_NOINIT;		
			}
			break;
		case DEV_SDCARD_MMC:
			stat &= ~STA_NOINIT;
			break;
		default:
			break;
	}
	return stat;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat = STA_NOINIT;

	switch (pdrv) {
		case DEV_SPI_NORFLASH :
			GD25Q64_init();
			stat = disk_status(DEV_SPI_NORFLASH);
			break;
		case DEV_SDCARD_MMC:
			if(SD_Init() == SD_OK) {
				stat &= ~STA_NOINIT;
			} else {
				stat = STA_NOINIT;
			}
			break;
		default:
			break;
	}
	return stat;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	DRESULT res = RES_PARERR;

	SD_Error SD_state = SD_OK;
	DWORD scratch[SD_BLOCKSIZE / 4];

	switch (pdrv) {
		case DEV_SPI_NORFLASH :
			/* 扇区偏移2MB，外部Flash文件系统空间放在SPI Flash后面6MB空间 */
			sector += 512;
			SPI_FLASH_BufferRead(buff, sector*4096, count*4096);
			res = RES_OK;
			break;
		case DEV_SDCARD_MMC:
			if((DWORD)buff & 3) {
				while (count--) {
					res = disk_read(DEV_SDCARD_MMC, (void *)scratch, sector++, 1);
					if (res != RES_OK) {
						break;
					}
					memcpy(buff, scratch, SD_BLOCKSIZE);
					buff += SD_BLOCKSIZE;
		    	}
			} else {
				SD_state = SD_ReadMultiBlocks(buff, (uint64_t)sector * SD_BLOCKSIZE, 
								SD_BLOCKSIZE, count);
				if(SD_state == SD_OK) {
					/* Check if the Transfer is finished */
					SD_state = SD_WaitReadOperation();
					while(SD_GetStatus() != SD_TRANSFER_OK);
				}
				res = SD_state == SD_OK ? RES_OK : RES_PARERR;
			}
			break;  
		default:
			break;
	}
	return res;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	DRESULT res = RES_PARERR;
	SD_Error SD_state = SD_OK;

	switch (pdrv) {
		case DEV_SPI_NORFLASH :
			/* 扇区偏移2MB，外部Flash文件系统空间放在SPI Flash后面6MB空间 */
			sector += 512;
			//一定要先擦除再写入
			SPI_FLASH_SectorErase(sector*4096);
			SPI_FLASH_BufferWrite((uint8_t *)buff, sector*4096, count*4096);
			res = RES_OK;
			break;
		case DEV_SDCARD_MMC:
			if((DWORD)buff & 3) {
				DRESULT res = RES_OK;
				DWORD scratch[SD_BLOCKSIZE / 4];

				while (count--) {
					memcpy(scratch, buff, SD_BLOCKSIZE);
					res = disk_write(DEV_SDCARD_MMC, (void *)scratch, sector++, 1);
					if (res != RES_OK) {
						break;
					}					
					buff += SD_BLOCKSIZE;
		    	}
			} else {
				SD_state = SD_WriteMultiBlocks((uint8_t *)buff, (uint64_t)sector * SD_BLOCKSIZE, 
									SD_BLOCKSIZE, count);
				if(SD_state == SD_OK) {
					/* Check if the Transfer is finished */
					SD_state = SD_WaitWriteOperation();
					/* Wait until end of DMA transfer */
					while(SD_GetStatus() != SD_TRANSFER_OK);			
				}
			}	
			res = SD_state == SD_OK ? RES_OK : RES_PARERR;
			break;
		default:
			break;
	}
	return res;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/
extern  SD_CardInfo SDCardInfo;
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;

	switch (pdrv) {
		case DEV_SPI_NORFLASH :
			switch(cmd)
			{
				//返回扇区个数
				case GET_SECTOR_COUNT:
					/* 扇区数量：1536*4096/1024/1024=6(MB) */
					*(DWORD*)buff = 1536;
					res = RES_OK;
					break;
				//返回每个扇区大小
				case GET_SECTOR_SIZE:
					*(WORD*)buff = 4096;
					res = RES_OK;
					break;
				//返回擦除扇区的最小个数（单位为扇区）
				case GET_BLOCK_SIZE:
					*(WORD*)buff = 1;	
					res = RES_OK;
					break;
				case CTRL_SYNC:
					res = RES_OK;
					break;
				default:
					res = RES_PARERR;
					break;
			}
			break;
		case DEV_SDCARD_MMC:
			switch (cmd) {
				// Get R/W sector size (WORD) 
				case GET_SECTOR_SIZE :    
					*(WORD*)buff = SD_BLOCKSIZE;
					res = RES_OK;
					break;
				// Get erase block size in unit of sector (DWORD)
				case GET_BLOCK_SIZE :      
					*(DWORD*)buff = 1;
					res = RES_OK;
					break;
				case GET_SECTOR_COUNT:
					*(DWORD*)buff = SDCardInfo.CardCapacity / SDCardInfo.CardBlockSize;
					res = RES_OK;
					break;
				case CTRL_SYNC :
					res = RES_OK;
					break;
				default:
					res = RES_PARERR;
					break;
			}
			break;
		default:
			res = RES_PARERR;
			break;
	}
	return res;
}

