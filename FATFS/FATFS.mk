FATFS_DIR = FATFS


FATFS_INCS = 	$(FATFS_DIR)

FATFS_SRCS = 	$(FATFS_DIR)/diskio.c \
				$(FATFS_DIR)/ff.c \
				$(FATFS_DIR)/ffsystem.c \
				$(FATFS_DIR)/ffunicode.c
				