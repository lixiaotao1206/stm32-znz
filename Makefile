
BIN = out/ZNZ_Board
BUILD_DIR = build
OBJS = 
SRCS = 
INCS = 

GCC = arm-none-eabi-gcc
LD = arm-none-eabi-ld
OBJDUMP = arm-none-eabi-objdump
OBJCOPY = arm-none-eabi-objcopy

#--specs=nano.specs --specs=nosys.specs
CFLAGS = -mcpu=cortex-m3 -mthumb -Wall -std=gnu99 -fno-builtin -Wno-unused-parameter -ffunction-sections -fdata-sections  -O0 -g -DUSE_STDPERIPH_DRIVER=1 -DSTM32F10X_HD=1
LDFLGS = -gc-sections 
IFLAGS = 
LINK_SCRIPT = LinkerScript/stm32_flash.ld

LIBGCC := $(shell $(GCC) --print-libgcc-file-name)

include Libraries/Libraries.mk
include App/App.mk
include FATFS/FATFS.mk

SRCS += $(LIBRARIES_SRCS) \
		$(FATFS_SRCS) \
		$(APP_SRCS) 
		

INCS += $(LIBRARIES_INCS) \
		$(FATFS_INCS) \
		$(APP_INCS)

IFLAGS += $(addprefix -I,$(INCS))

OBJS_NO_PREFIX_S += $(subst .c,.o,$(SRCS))
OBJS_NO_PREFIX += $(subst .s,.o,$(OBJS_NO_PREFIX_S))
OBJS += $(addprefix $(BUILD_DIR)/,$(OBJS_NO_PREFIX))

$(BIN):$(OBJS)
	mkdir -p out
	$(LD) -T$(LINK_SCRIPT) $^ $(LDFLGS) $(LIBGCC) -o $(BIN).elf
	$(OBJCOPY) -O binary -S $(BIN).elf $(BIN).bin
	$(OBJDUMP) -D -m arm $(BIN).elf > $(BIN).dis

$(BUILD_DIR)/%.o : %.s 
	mkdir -p $(subst /nouse,,$(BUILD_DIR)/$(subst $(notdir $<),nouse,$<))
	$(GCC) -c $(CFLAGS) $(IFLAGS) $< -o $@ 

$(BUILD_DIR)/%.o : %.c
	mkdir -p $(subst /nouse,,$(BUILD_DIR)/$(subst $(notdir $<),nouse,$<))
	$(GCC) $(CFLAGS) $(IFLAGS) -c $< -o $@

clean:
	rm out build -rf
