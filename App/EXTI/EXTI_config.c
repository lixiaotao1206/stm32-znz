#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "app_config.h"
#include "EXTI_config.h"

void EXTI_config(void)
{
	EXTI_InitTypeDef EXIT_td;
	NVIC_InitTypeDef nvic_td;
	//打开时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); 
	//配置中断优先级
	nvic_td.NVIC_IRQChannel = EXTI0_IRQn;
	nvic_td.NVIC_IRQChannelPreemptionPriority = 1;
	nvic_td.NVIC_IRQChannelSubPriority = 1;
	nvic_td.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_td);
	//把KEY1关联到EXIT0
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
	//外部终端配置
	EXIT_td.EXTI_Line = EXTI_Line0;
	EXIT_td.EXTI_Mode = EXTI_Mode_Interrupt;
	EXIT_td.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXIT_td.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXIT_td);
}

void EXTI0_IRQHandler(void)
{
	if (GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == RESET) {
		LED_set_state(LED_RED);
	} else {
		LED_set_state(LED_BLACK);
	}
	EXTI_ClearFlag(EXTI_Line0);
}