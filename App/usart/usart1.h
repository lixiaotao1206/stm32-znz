#ifndef __USART_1_H
#define __USART_1_H

extern void USART1_init(void);
extern void USART1_send_data(uint8_t *data, uint16_t len);
extern void USART_DMA_test(void);

#endif