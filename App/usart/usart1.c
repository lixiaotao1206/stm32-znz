#include <stdint.h>
#include <stdio.h>
#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "led.h"
#include "bsp_delay.h"
#include "usart1.h"
#include "printf.h"

static uint8_t buff[] = {'D','M','A',':','0','1','2','3','4','5','\n'};

void USART1_init(void)
{
    USART_InitTypeDef usart_cfg;
    GPIO_InitTypeDef gpio_cfg;
    NVIC_InitTypeDef nvic_td;
    DMA_InitTypeDef dma_cfg;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    // MCU-TXD GPIO9
    gpio_cfg.GPIO_Pin = GPIO_Pin_9;
    gpio_cfg.GPIO_Mode = GPIO_Mode_AF_PP;   /* AF 推挽 */
    gpio_cfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio_cfg);
    // MCU_RXD GPIO10
    gpio_cfg.GPIO_Pin = GPIO_Pin_10;
    gpio_cfg.GPIO_Mode = GPIO_Mode_IN_FLOATING; /* 浮空（高阻） */
    gpio_cfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio_cfg);
    // 串口配置
    usart_cfg.USART_BaudRate = 115200;
    usart_cfg.USART_WordLength = USART_WordLength_8b;
    usart_cfg.USART_StopBits = USART_StopBits_1;
    usart_cfg.USART_Parity = USART_Parity_No;
    usart_cfg.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    usart_cfg.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART1, &usart_cfg);
    // 配置串口1中断
    nvic_td.NVIC_IRQChannel = USART1_IRQn;
	nvic_td.NVIC_IRQChannelPreemptionPriority = 1;
	nvic_td.NVIC_IRQChannelSubPriority = 2;
	nvic_td.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_td);
    NVIC_EnableIRQ(USART1_IRQn);
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    // LED 初始化
    LED_config();
    // DMA 初始化
    dma_cfg.DMA_PeripheralBaseAddr = (uint32_t)&(USART1->DR);
    dma_cfg.DMA_MemoryBaseAddr = (uint32_t)buff;
    dma_cfg.DMA_DIR = DMA_DIR_PeripheralDST;
    dma_cfg.DMA_BufferSize = sizeof(buff);
    dma_cfg.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    dma_cfg.DMA_MemoryInc = DMA_MemoryInc_Enable;
    dma_cfg.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    dma_cfg.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    dma_cfg.DMA_Mode = DMA_Mode_Normal;
    dma_cfg.DMA_Priority = DMA_Priority_High;
    dma_cfg.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel4, &dma_cfg);
    // 配置DMA1-4中断
    nvic_td.NVIC_IRQChannel = DMA1_Channel4_IRQn;
	nvic_td.NVIC_IRQChannelPreemptionPriority = 1;
	nvic_td.NVIC_IRQChannelSubPriority = 3;
	nvic_td.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic_td);
    NVIC_EnableIRQ(DMA1_Channel4_IRQn);
    DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
    // 使能串口1
    USART_Cmd(USART1, ENABLE);
    // 使能DMA
    DMA_ClearFlag(DMA1_FLAG_TC4);
    DMA_Cmd(DMA1_Channel4, ENABLE);
    
}

void USART1_send_data(uint8_t *data, uint16_t len)
{
    uint16_t index = 0;
    while (index < len) {
        LED_set_state(LED_BLACK);
        USART_SendData(USART1, data[index]);
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
        index++;
    }
}

void USART_DMA_test(void)
{
    uint8_t buff[] = {'A', 'B', 'C', 'D', '1', '2', '3', '4', 10};
    printf( "this is usart1 test\r\n");
    USART1_send_data(buff, sizeof(buff));
    printf( "this is DMA test\r\n");
    //DMA
    USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    // while (1) {
    //     DMA_ClearFlag(DMA1_FLAG_TC4);
    //     DMA_Cmd(DMA1_Channel4, DISABLE);
    //     DMA_Init(DMA1_Channel4, &dma_cfg);
    //     DMA_Cmd(DMA1_Channel4, ENABLE);
    //     USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
    //     bsp_delay_ms(500);
    // }
}

int putchar(int ch)
{
    USART1_send_data((uint8_t*)&ch, 1);
    return ch;
}

int _dputs(const char *string)
{
    char *p = (char*)string;
    uint32_t len = 0;

    while (*p != '\0' && len < 64) {
        USART_SendData(USART1, *p++);
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
        len++;
    }
    return len;
}


void USART1_IRQHandler(void)
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET) {
        LED_set_state(LED_GREEN);
        //USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        USART_SendData(USART1, USART_ReceiveData(USART1));
        //LED_set_state(LED_BLACK);
    } 
}

void DMA1_Channel4_IRQHandler(void)
{
    if (DMA_GetITStatus(DMA1_IT_TC4) == SET) {
        LED_set_state(LED_YELLOW);
        DMA_ClearITPendingBit(DMA1_IT_TC4);
        LED_set_state(LED_BLACK);
        
    }
}

