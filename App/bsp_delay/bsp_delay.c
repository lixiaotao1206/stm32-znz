#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "core_cm3.h"
#include "sys_clock.h"
#include "bsp_delay.h"

#define SYSTICK SYSCLK_get_clock()

static uint32_t it_counter = 0;

void SysTick_Handler(void)
{
    it_counter++;
}

void bsp_delay_us(uint32_t us)
{
    it_counter = 0;
    //打开并配置systick定时器
    SysTick_Config(SYSTICK/1000000);
    while (it_counter < us) {
        
    }
    //关闭systick定时器
    SysTick->CTRL &= (~SysTick_CTRL_ENABLE_Msk);
}

void bsp_delay_ms(uint32_t ms)
{
    it_counter = 0;
    //打开并配置systick定时器
    SysTick_Config(SYSTICK/1000);
    while (it_counter < ms) {
        
    }
    //关闭systick定时器
    SysTick->CTRL &= (~SysTick_CTRL_ENABLE_Msk);
}