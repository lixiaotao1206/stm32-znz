#ifndef __BSP_DELAY_H
#define __BSP_DELAY_H

#include <stdint.h>

extern void bsp_delay_us(uint32_t us);
extern void bsp_delay_ms(uint32_t ms);

#endif