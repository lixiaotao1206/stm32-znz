#ifndef __FAT_TEST_H
#define __FAT_TEST_H

#include "ff.h"

#include "printf.h"
#define FAT_PRINT printf

typedef enum{
    DISK_GD25Q64 = 0,
    DISK_SD,
    DISK_MAX,
}FAT_DISK_t;

extern void fatfs_test(FAT_DISK_t disk_type);

#endif
