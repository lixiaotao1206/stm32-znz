#include "fattest.h"
#include <stddef.h>

void fatfs_test(FAT_DISK_t disk_type)
{
    FRESULT res;
    FATFS fsObject;
    char dev_path[3] = "0:";
    char file_name[32] = "0:abc_long_long_name.txt";
    FIL fp; 
    const char wData[] = "FATFS test";
    char rData[4096] = "";
    UINT bw, br;


    if (disk_type == DISK_GD25Q64){
        dev_path[0] = '0';
        file_name[0] = '0';
    } else if (disk_type == DISK_SD){
        dev_path[0] = '1';
        file_name[0] = '1';
    } else {
        return;
    }
    //挂载文件系统
	res = f_mount(&fsObject, dev_path, 1);
	FAT_PRINT("f_mount res = %d\r\n", res);
	if(res == FR_NO_FILESYSTEM) {
		res = f_mkfs(dev_path, 0, rData, 512);
		FAT_PRINT("f_mkfs res = %d\r\n", res);
		//格式化后要取消挂载再重新挂载文件系统
		res = f_mount(NULL, dev_path, 1);
		res = f_mount(&fsObject, dev_path, 1);
		FAT_PRINT("second f_mount res = %d\r\n", res);
	}
    if(res != FR_OK) {
        //return;
    }
	res = f_open(&fp, file_name, FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
	FAT_PRINT("f_open res = %d\r\n",res);
	if(res == FR_OK)
	{
		res = f_write(&fp, wData, sizeof(wData), &bw);
		FAT_PRINT("write: %s, bw = %d\r\n", wData, bw);		
		if(res == FR_OK)
		{
			f_lseek(&fp,0);
			res = f_read(&fp, rData, f_size(&fp), &br);
			if(res == FR_OK) {
				FAT_PRINT("read : %s, br = %d\r\n", rData, (int)br);	
            }	
		}	
		f_close(&fp);
	}
	f_unmount(dev_path);
}