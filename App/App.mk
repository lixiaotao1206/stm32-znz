APP_DIR = App

include $(APP_DIR)/key/key.mk
include $(APP_DIR)/led/led.mk
include $(APP_DIR)/NVIC/NVIC.mk
include $(APP_DIR)/EXTI/EXTI_config.mk
include $(APP_DIR)/sys_clock/sys_clock.mk
include $(APP_DIR)/bsp_delay/bsp_delay.mk
include $(APP_DIR)/usart/usart.mk
include $(APP_DIR)/AT24C02-I2C/at24c02-i2c.mk
include $(APP_DIR)/GD25Q64-SPI/gd25q64-spi.mk
include $(APP_DIR)/FAT_test/FAT_test.mk
include $(APP_DIR)/SDCard-SDIO/sdcard_sdio.mk
include $(APP_DIR)/adc/adc.mk



APP_INCS = $(APP_DIR) \
			$(KEY_INCS) \
			$(LED_INCS) \
			$(NVIC_INCS) \
			$(EXTI_CONFIG_INCS)\
			$(SYS_CLOCK_INCS) \
			$(BSP_DELAY_INCS) \
			$(USART_INCS) \
			$(AT24C02-I2C_INCS) \
			$(GD25Q64-SPI_INCS) \
			$(FAT_TEST_INCS) \
			$(SDCARD_SDIO_INCS) \
			$(ADC_INCS)

APP_SRCS = $(APP_DIR)/main.c \
			$(KEY_SRCS) \
			$(LED_SRCS) \
			$(NVIC_SRCS) \
			$(EXTI_CONFIG_SRCS) \
			$(SYS_CLOCK_SRCS) \
			$(BSP_DELAY_SRCS) \
			$(USART_SRCS) \
			$(AT24C02-I2C_SRCS) \
			$(GD25Q64-SPI_SRCS) \
			$(FAT_TEST_SRCS) \
			$(SDCARD_SDIO_SRCS) \
			$(ADC_SRCS)