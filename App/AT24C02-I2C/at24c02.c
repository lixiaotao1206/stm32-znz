#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "at24c02.h"

void AT24C02_init(void)
{
    I2C_InitTypeDef I2C_cfg;
    GPIO_InitTypeDef gpio_cfg;
    //打开时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    //GPIO初始化(SCL SDA)
    gpio_cfg.GPIO_Speed = GPIO_Speed_50MHz;
    gpio_cfg.GPIO_Mode = GPIO_Mode_AF_OD;
    gpio_cfg.GPIO_Pin = GPIO_Pin_6;
    GPIO_Init(GPIOB, &gpio_cfg);
    gpio_cfg.GPIO_Pin = GPIO_Pin_7;
    GPIO_Init(GPIOB, &gpio_cfg);
    //I2C初始化
    I2C_cfg.I2C_ClockSpeed = 400000;    //400K
    I2C_cfg.I2C_Mode = I2C_Mode_I2C;
    I2C_cfg.I2C_DutyCycle = I2C_DutyCycle_2;
    I2C_cfg.I2C_OwnAddress1 = 0xAA;
    I2C_cfg.I2C_Ack = I2C_Ack_Enable;
    I2C_cfg.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2C_Init(I2C1, &I2C_cfg);
    I2C_Cmd(I2C1, ENABLE);
}

int32_t AT24C02_write_page(uint8_t addr, uint8_t *data, uint8_t len)
{
    int32_t timeout = I2C_TIMEOUT_NUM * 20;
    //等待空闲
    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)) {
        if(timeout-- == 0) {
            I2C_PRINT("busy error\r\n");
            return -1;
        }
    }
    //起始信号
    I2C_GenerateSTART(I2C1, ENABLE);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("start error\r\n");
            return -2;
        }
    }
    //I2C地址
    I2C_Send7bitAddress(I2C1, 0xA0, I2C_Direction_Transmitter);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("I2C Addr send error\r\n");
            return -3;
        }
    }
    //EEPROM地址
    I2C_SendData(I2C1, addr);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("eeprom Addr send error\r\n");
            return -4;
        }
    }
    //数据
    while(len-- > 0){
        I2C_SendData(I2C1, *data++);
        timeout = I2C_TIMEOUT_NUM;
        while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) != SUCCESS) {
            if(timeout-- == 0) {
                I2C_PRINT("eeprom data send error\r\n");
                return -5;
            }
        }
    }
    //停止信号
    I2C_GenerateSTOP(I2C1, ENABLE);
    return 0;
}

void I2C_wait_standby(void)      
{
    volatile uint16_t SR1_Tmp = 0;
    //读SR1寄存器，再向DR寄存器写，就会清除BTF位，此位表示字节发送结束
    do {
        I2C_GenerateSTART(I2C1, ENABLE);
        SR1_Tmp = I2C_ReadRegister(I2C1, I2C_Register_SR1);
        (void)SR1_Tmp;
        I2C_Send7bitAddress(I2C1, 0xA0, I2C_Direction_Transmitter);
    }while(!(I2C_ReadRegister(I2C1, I2C_Register_SR1) & 0x0002));
    //清除应答失败位
    I2C_ClearFlag(I2C1, I2C_FLAG_AF); 
    I2C_GenerateSTOP(I2C1, ENABLE); 
}

int32_t AT24C02_write_buffer(uint8_t addr, uint8_t* buffer, uint16_t len)
{
    uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0;

    Addr = addr % AT24C02_PAGE_SIZE;
    count = AT24C02_PAGE_SIZE - Addr;
    NumOfPage =  len / AT24C02_PAGE_SIZE;
    NumOfSingle = len % AT24C02_PAGE_SIZE;
    
    /* If addr is AT24C02_PAGE_SIZE aligned  */
    if(Addr == 0) {
        /* If len < AT24C02_PAGE_SIZE */
        if(NumOfPage == 0) {
            AT24C02_write_page(addr, buffer, NumOfSingle);
            I2C_wait_standby();
        }
        /* If len > AT24C02_PAGE_SIZE */
        else {
            while(NumOfPage--) {
                AT24C02_write_page(addr, buffer, AT24C02_PAGE_SIZE); 
                I2C_wait_standby();
                addr +=  AT24C02_PAGE_SIZE;
                buffer += AT24C02_PAGE_SIZE;
            }
            if(NumOfSingle!=0) {
                AT24C02_write_page(addr, buffer, NumOfSingle);
                I2C_wait_standby();
            }
        }
    }
    /* If addr is not AT24C02_PAGE_SIZE aligned  */
    else {
        /* If len < AT24C02_PAGE_SIZE */
        if(NumOfPage== 0) {
            AT24C02_write_page(addr, buffer, NumOfSingle);
            I2C_wait_standby();
        }
        /* If len > AT24C02_PAGE_SIZE */
        else {
            len -= count;
            NumOfPage =  len / AT24C02_PAGE_SIZE;
            NumOfSingle = len % AT24C02_PAGE_SIZE;	
            if(count != 0) {  
                AT24C02_write_page(addr, buffer, count);
                I2C_wait_standby();
                addr += count;
                buffer += count;
            } 
            while(NumOfPage--) {
                AT24C02_write_page(addr, buffer, AT24C02_PAGE_SIZE);
                I2C_wait_standby();
                addr +=  AT24C02_PAGE_SIZE;
                buffer += AT24C02_PAGE_SIZE;  
            }
            if(NumOfSingle != 0) {
                AT24C02_write_page(addr, buffer, NumOfSingle); 
                I2C_wait_standby();
            }
        }
    } 
    return 0;
}

int32_t AT24C02_read_buffer(uint8_t addr, uint8_t* buffer, uint16_t len)
{  
  
    volatile uint16_t SR1_Tmp, SR2_Tmp;
    int32_t timeout = I2C_TIMEOUT_NUM * 20;

    while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)) {
        if(timeout-- == 0) {
            I2C_PRINT("busy error\r\n");
            return -1;
        }
    }
    //发送起始信号
    I2C_GenerateSTART(I2C1, ENABLE);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("start error\r\n");
            return -2;
        }
    }
    //I2C地址
    I2C_Send7bitAddress(I2C1, 0xA0, I2C_Direction_Transmitter);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("I2C Addr send error\r\n");
            return -3;
        }
    }
    // 通过读SR1再读SR2清除 EV6 
    SR1_Tmp = I2C_ReadRegister(I2C1, I2C_Register_SR1);
    SR2_Tmp = I2C_ReadRegister(I2C1, I2C_Register_SR2);
    (void)SR1_Tmp; (void)SR2_Tmp; 
    // 发送read地址
    I2C_SendData(I2C1, addr);  
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("eeprom Addr send error\r\n");
            return -4;
        }
    }
    //重新发送起始信号 
    I2C_GenerateSTART(I2C1, ENABLE);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("start error\r\n");
            return -5;
        }
    }
    // 发送EEPROM地址，接收
    I2C_Send7bitAddress(I2C1, 0xA0, I2C_Direction_Receiver);
    timeout = I2C_TIMEOUT_NUM;
    while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) != SUCCESS) {
        if(timeout-- == 0) {
            I2C_PRINT("recv error\r\n");
            return -6;
        }
    }
    //读数据
    while(len) {
        if(len == 1)
        {
            //关闭回应信号
            I2C_AcknowledgeConfig(I2C1, DISABLE);
            //停止
            I2C_GenerateSTOP(I2C1, ENABLE);
        }
        // Test on EV7 and clear it   
        timeout = I2C_TIMEOUT_NUM;
		while(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED) == ERROR) {
			if(timeout-- == 0) {
                I2C_PRINT("recv over error\r\n");
                return -2;
            }
		}  
        //read 数据
        *buffer = I2C_ReceiveData(I2C1);
        buffer++; 
        len--;         
    }
    // Enable Acknowledgement to be ready for another reception 
    I2C_AcknowledgeConfig(I2C1, ENABLE);
    return 0;
}

void AT24C02_test(void)
{
    int8_t i = 0;
    uint8_t buff_w[23] = "Hello AT24C02-I2C Test";
    uint8_t buff_r[23];

    AT24C02_init();
    if (AT24C02_write_buffer(0, buff_w, sizeof(buff_w)) != 0) {
        I2C_PRINT("AT24C02-I2C test write error\r\n");
        return;
    } 
    I2C_PRINT("AT24C02-I2C test write:%s\r\n",buff_w);
    if (AT24C02_read_buffer(0, buff_r, sizeof(buff_r)) != 0) {
        I2C_PRINT("AT24C02-I2C test read error\r\n");
        return;
    }
    I2C_PRINT("AT24C02-I2C test read :%s\r\n",buff_r);
    for(i=0; i<sizeof(buff_r); i++) {
        if(buff_w[i] != buff_r[i]) {
            I2C_PRINT("AT24C02-I2C test error!\r\n");
            return;
        }
    }
    I2C_PRINT("AT24C02 test success!\r\n");
}