#ifndef _AT24C02_H
#define _AT24C02_H

#include "printf.h"
#define I2C_PRINT               printf


#define I2C_TIMEOUT_NUM         0x1000
#define AT24C02_PAGE_SIZE       8

extern void AT24C02_init(void);
extern int32_t AT24C02_write_buffer(uint8_t addr, uint8_t* buffer, uint16_t len);
extern int32_t AT24C02_read_buffer(uint8_t addr, uint8_t* buffer, uint16_t len);
extern void AT24C02_test(void);

#endif
