#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "key.h"

void key_config(void)
{
	//配置GPIO，KEY1-PA0
	GPIO_InitTypeDef PA0_td;
	
	PA0_td.GPIO_Pin = GPIO_Pin_0;
	PA0_td.GPIO_Speed = GPIO_Speed_50MHz;
	PA0_td.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_Init(GPIOA, &PA0_td);
}