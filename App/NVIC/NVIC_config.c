#include "NVIC_config.h"
#include "stm32f10x.h"
#include "stm32f10x_conf.h"

void NVIC_priority_group_config(void)
{
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
}