#ifndef __BSP_LED_H
#define __BSP_LED_H

typedef enum {
    LED_BLACK = 0,  //0
    LED_RED,        //R
    LED_GREEN,      //G
    LED_BLUE,       //B
    LED_YELLOW,     //RG
    LED_PURPLE,     //RB
    LED_CYAN,       //GB
    LED_WHITE,      //RGB
} LED_t;

typedef enum {
    LED_OFF = 0,
    LED_ON = 1,
} LED_STATE_t;

extern void LED_config(void);
extern void LED_set_state(LED_t led);
extern LED_STATE_t LED_get_state(void);
extern void LED_poll(void);

#endif /* __BSP_LED_H */

