#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "led.h"

void LED_config(void)
{
	// 配置GPIO，LEDG-PB0
	GPIO_InitTypeDef led_cfg;
	// 第一步一定要打开时钟，否则下面的初始化无效
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	// 三个LED GPIO配置
	led_cfg.GPIO_Speed = GPIO_Speed_50MHz;
	led_cfg.GPIO_Mode = GPIO_Mode_Out_PP;
	// red led
	led_cfg.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOB, &led_cfg);
	// green led
	led_cfg.GPIO_Pin = GPIO_Pin_0;
	GPIO_Init(GPIOB, &led_cfg);
	// blue led
	led_cfg.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOB, &led_cfg);

	// 初始化LED全关闭
	GPIO_WriteBit(GPIOB, GPIO_Pin_5, Bit_SET);
	GPIO_WriteBit(GPIOB, GPIO_Pin_0, Bit_SET);
	GPIO_WriteBit(GPIOB, GPIO_Pin_1, Bit_SET);
}

void LED_set_state(LED_t led)
{
	uint16_t odr = 0;

	odr = GPIO_ReadOutputData(GPIOB) & (~(GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1));
	switch (led) {
		case LED_BLACK:
			odr |= (GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1);
			break;
		case LED_RED:
			odr |= (GPIO_Pin_0 | GPIO_Pin_1);
			break;
		case LED_GREEN:
			odr |= (GPIO_Pin_5 | GPIO_Pin_1);
			break;	
		case LED_BLUE:
			odr |= (GPIO_Pin_5 | GPIO_Pin_0);
			break;
		case LED_YELLOW:
			odr |= GPIO_Pin_1;
			break;
		case LED_PURPLE:
			odr |= GPIO_Pin_0;
			break;	
		case LED_CYAN:
			odr |= GPIO_Pin_5;
			break;
		case LED_WHITE:
			break;
		default:
			odr |= (GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1);
			break;
	}
	GPIO_Write(GPIOB, odr);
}

LED_STATE_t LED_get_state(void)
{
	uint16_t odr = 0;
	LED_t state = (GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1);

	odr = GPIO_ReadOutputData(GPIOB) & (GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1);
	switch (odr) {
		case (GPIO_Pin_5 | GPIO_Pin_0 | GPIO_Pin_1):
			state = LED_BLACK;
			break;
		case (GPIO_Pin_0 | GPIO_Pin_1):
			state = LED_RED;
			break;
		case (GPIO_Pin_5 | GPIO_Pin_1):
			state = LED_GREEN;
			break;
		case (GPIO_Pin_5 | GPIO_Pin_0):
			state = LED_BLUE;
			break;
		case (GPIO_Pin_1):
			state = LED_YELLOW;
			break;
		case (GPIO_Pin_0):
			state = LED_PURPLE;
			break;
		case (GPIO_Pin_5):
			state = LED_CYAN;
			break;
		case (0):
			state = LED_WHITE;
			break;
		default:
			break;
	}
	return state;
}

void LED_poll(void)
{
	static LED_t state = LED_BLACK;
	static uint8_t off = 1;

	if (off == 0) {
		off = 1;
		LED_set_state(LED_BLACK);
	} else {
		off = 0;
		LED_set_state(state);
		if (state++ == LED_WHITE) {
			state = LED_RED;
		}
	}
}