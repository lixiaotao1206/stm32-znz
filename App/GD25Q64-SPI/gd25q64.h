#ifndef _GD25Q64_H
#define _GD25Q64_H

#include "printf.h"
#define SPI_PRINT                       printf

#define SPIT_FLAG_TIMEOUT               ((uint32_t)0x1000)
#define SPIT_LONG_TIMEOUT               ((uint32_t)(10 * SPIT_FLAG_TIMEOUT))

#define  sFLASH_ID                      0XEF4017    //W25Q64

#define SPI_FLASH_PageSize              256
#define SPI_FLASH_PerWritePageSize      256


#define W25X_WriteEnable		        0x06 
#define W25X_WriteDisable		        0x04 
#define W25X_ReadStatusReg		        0x05 
#define W25X_WriteStatusReg		        0x01 
#define W25X_ReadData			        0x03 
#define W25X_FastReadData		        0x0B 
#define W25X_FastReadDual		        0x3B 
#define W25X_PageProgram		        0x02 
#define W25X_BlockErase			        0xD8 
#define W25X_SectorErase		        0x20 
#define W25X_ChipErase			        0xC7 
#define W25X_PowerDown			        0xB9 
#define W25X_ReleasePowerDown	        0xAB 
#define W25X_DeviceID			        0xAB 
#define W25X_ManufactDeviceID   	    0x90 
#define W25X_JedecDeviceID		        0x9F

/* WIP(busy)标志，FLASH内部正在写入 */
#define WIP_Flag                        0x01
#define Dummy_Byte                      0xFF


extern void GD25Q64_init(void);
extern void SPI_FLASH_WriteEnable(void);
extern void SPI_Flash_PowerDown(void);
extern void SPI_Flash_WAKEUP(void);
extern void SPI_FLASH_SectorErase(uint32_t SectorAddr);
extern void SPI_FLASH_BulkErase(void);
extern void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
extern void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
extern uint32_t SPI_FLASH_ReadID(void);
extern uint32_t SPI_FLASH_ReadDeviceID(void);
extern void SPI_FLASH_StartReadSequence(uint32_t ReadAddr);

extern void GD25Q64_test(void);

#endif