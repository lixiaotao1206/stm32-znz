#include "stm32f10x.h"
#include "sys_clock.h"

void SYSCLK_config_HSE(SYSTICK_t systick)
{
	RCC_DeInit();
	RCC_HSEConfig(RCC_HSE_ON);
	while (RCC_WaitForHSEStartUp() != SUCCESS) ;
	if (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == SET) {

			FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
			if (systick >= SYSTICK_48M) {
				FLASH_SetLatency(FLASH_Latency_2);
			} else if (systick >= SYSTICK_24M) {
				FLASH_SetLatency(FLASH_Latency_1);
			} else {
				FLASH_SetLatency(FLASH_Latency_0);
			}
			
			RCC_HCLKConfig(RCC_SYSCLK_Div1);
			RCC_PCLK1Config(RCC_HCLK_Div2);
			RCC_PCLK2Config(RCC_HCLK_Div1);
		
			RCC_PLLConfig(RCC_PLLSource_HSE_Div1, systick);
			RCC_PLLCmd(ENABLE);
		
			while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) != SET) ;
			
			RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
			while (RCC_GetSYSCLKSource() != 0x08) ;
	} else {
		
	}
}

void SYSCLK_config_HSI_8M(void)
{
	RCC_DeInit();
	RCC_HSICmd(ENABLE);

	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
	FLASH_SetLatency(FLASH_Latency_0);
	
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div1);
	RCC_PCLK2Config(RCC_HCLK_Div1);
	
	RCC_PLLCmd(DISABLE);
			
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

	while (RCC_GetSYSCLKSource() != 0x00) {

	}
}

uint32_t SYSCLK_get_clock(void)
{
	uint32_t clock = 0;

	if (RCC_GetSYSCLKSource() == 0x00) {
		return 8000000;
	} else {
		switch (RCC->CFGR & 0x003C0000) {
			case SYSTICK_72M:
				clock = 72000000;
				break;
			case SYSTICK_64M:
				clock = 64000000;
				break;
			case SYSTICK_56M:
				clock = 56000000;
				break;
			case SYSTICK_48M:
				clock = 48000000;
				break;
			case SYSTICK_40M:
				clock = 40000000;
				break;
			case SYSTICK_32M:
				clock = 32000000;
				break;
			case SYSTICK_24M:
				clock = 24000000;
				break;
			case SYSTICK_16M:
				clock = 16000000;
				break;
			default:
				break;
		}
	}
	return clock;
}