#ifndef __SYS_CLOCK_H
#define __SYS_CLOCK_H

#include "stm32f10x_conf.h"

typedef enum {
    SYSTICK_72M = RCC_PLLMul_9,
    SYSTICK_64M = RCC_PLLMul_8,
    SYSTICK_56M = RCC_PLLMul_7,
    SYSTICK_48M = RCC_PLLMul_6,
    SYSTICK_40M = RCC_PLLMul_5,
    SYSTICK_32M = RCC_PLLMul_4,
    SYSTICK_24M = RCC_PLLMul_3,
    SYSTICK_16M = RCC_PLLMul_2
}SYSTICK_t;

extern void SYSCLK_config_HSE(SYSTICK_t systick);
extern void SYSCLK_config_HSI_8M(void);
extern uint32_t SYSCLK_get_clock(void);

#endif