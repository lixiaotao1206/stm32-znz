#include "app_config.h"

int main(void)
{
	//配置时钟树
	SYSCLK_config_HSE(SYSTICK_72M);
	//配置NVIC优先级组
	NVIC_priority_group_config();
	//KEY初始化
	key_config();
	//EXIT初始化
	EXTI_config();
	//LED初始化
	LED_config();
	// 串口1初始化
	USART1_init();
	// 串口 DMA test
	USART_DMA_test();

#if 0
	AT24C02_test();
#endif

#if 0
	GD25Q64_test();
#endif

#if 0
	fatfs_test(DISK_GD25Q64);
#endif

#if 0
	fatfs_test(DISK_SD);
#endif

#if 1
	ADC_test();
#endif

	while(1) {
		bsp_delay_ms(1000);
		LED_poll();
	}
}

