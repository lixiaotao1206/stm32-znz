#include "string.h"

void *memcpy(void *destin, void const *source, size_t n)
{
    char *d = (char*)destin;
    const char *s = (const char*)source;

    while(n--) {
        *d = *s;
        d++;
        s++;
    }
    return (void*)destin;
}

void *memset(void *s, int ch, size_t n)
{
    char *buff = (char*)s;
    while(n--) {
        *buff = ch & 0xff;
        buff++;
    }
    return (void*)s;
}

void* mymemcpy(void* des, const void* src, size_t num)
{
	void* ret = des;
	//复制
	while (num--) {
		*(char*)des = *(const char*)src;
		des = (char*)des + 1;//这里不采用++ 是因为进行了强制类型转换 不可以直接++！！
		src = (char*)src + 1;
	}
	return ret;
}

int memcmp(void const *s1, const void *s2, size_t len)  
{  
    const char* str1 = (const char*)s1;
    const char* str2 = (const char*)s2;

    while(len--) {  
        while(*str1 == *str2) {  
        if(*str1=='\0') { 
            return 0;  
            }
            str1++;  
            str2++;  
        }  
    }  
    if(*str1 > *str2) {
        return 1;  
    }  
    if(*str1 < *str2) {
        return -1;
    } 
    return -1;
}  

char* strchr(char const *s, int c)
{
    char* str = (char*)s;

	while (*str && *str != (char)c) {
		str++;
	}
	if (*str == (char)c) {
		return (char*)str;
	}
	return NULL;
}
