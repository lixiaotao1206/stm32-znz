LIBC_DIR = $(LIBRARIES_DIR)/Libc

LIBC_SRCS = $(LIBC_DIR)/printf.c \
			$(LIBC_DIR)/strlen.c \
			$(LIBC_DIR)/ctype.c \
			$(LIBC_DIR)/string.c 

LIBC_INCS = $(LIBC_DIR)/include