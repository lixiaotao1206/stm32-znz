CMSIS_DIR = $(LIBRARIES_DIR)/CMSIS

CMSIS_INCS = $(CMSIS_DIR)
CMSIS_SRCS = $(CMSIS_DIR)/startup/startup_stm32f10x_hd.s \
				$(CMSIS_DIR)/core_cm3.c \
				$(CMSIS_DIR)/system_stm32f10x.c 