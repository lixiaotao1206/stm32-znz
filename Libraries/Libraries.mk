LIBRARIES_DIR = Libraries

include $(LIBRARIES_DIR)/CMSIS/CMSIS.mk
include $(LIBRARIES_DIR)/Libc/libc.mk
include $(LIBRARIES_DIR)/STM32F10x_StdPeriph_Driver/DRIVER.mk

LIBRARIES_INCS = 	$(CMSIS_INCS) \
					$(LIBC_INCS) \
					$(DRIVER_INCS)

LIBRARIES_SRCS = 	$(CMSIS_SRCS) \
					$(LIBC_SRCS) \
					$(DRIVER_SRCS)