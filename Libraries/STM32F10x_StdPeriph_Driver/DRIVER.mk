DRIVER_DIR = $(LIBRARIES_DIR)/STM32F10x_StdPeriph_Driver

DRIVER_INCS = $(DRIVER_DIR)/inc
DRIVER_SRCS = $(wildcard $(DRIVER_DIR)/src/*.c)
# DRIVER_SRCS = $(DRIVER_DIR)/src/misc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_adc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_bkp.c \
# 				$(DRIVER_DIR)/src/stm32f10x_can.c \
# 				$(DRIVER_DIR)/src/stm32f10x_cec.c \
# 				$(DRIVER_DIR)/src/stm32f10x_crc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_dac.c \
# 				$(DRIVER_DIR)/src/stm32f10x_dbgmcu.c \
# 				$(DRIVER_DIR)/src/stm32f10x_dma.c \
# 				$(DRIVER_DIR)/src/stm32f10x_exti.c \
# 				$(DRIVER_DIR)/src/stm32f10x_flash.c \
# 				$(DRIVER_DIR)/src/stm32f10x_fsmc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_gpio.c \
# 				$(DRIVER_DIR)/src/stm32f10x_i2c.c \
# 				$(DRIVER_DIR)/src/stm32f10x_iwdg.c \
# 				$(DRIVER_DIR)/src/stm32f10x_pwr.c \
# 				$(DRIVER_DIR)/src/stm32f10x_rcc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_rtc.c \
# 				$(DRIVER_DIR)/src/stm32f10x_stdio.c \
# 				$(DRIVER_DIR)/src/stm32f10x_spi.c \
# 				$(DRIVER_DIR)/src/stm32f10x_tim.c \
# 				$(DRIVER_DIR)/src/stm32f10x_usart.c \
# 				$(DRIVER_DIR)/src/stm32f10x_wwdg.c 